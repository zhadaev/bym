export enum ProductTypes {
  BASIC_SINGLE = 'Basic single',
  BASIC_VALID_HOURS = 'Basic valid hours',
  PASS_LIMITED = 'Basic limited',
  PASS_UNLIMITED = 'Pass unlimited'
}

export enum CustomerTypes {
  ADULT = 'Adult',
  CHILD = 'Child',
  SENIOR = 'Senior'
}
