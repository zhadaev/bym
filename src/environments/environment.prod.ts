import { ProductTypes, CustomerTypes } from './config';

export const environment = {
  production: true,
  api: {
    products: 'https://bym-db.herokuapp.com/products'
  },
  productTypes: ProductTypes,
  customerTypes: CustomerTypes
};
