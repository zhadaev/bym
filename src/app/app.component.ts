import { environment as env } from '../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductsService } from './services/products.service';
import { Product } from './shared/product.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  products$: Observable<Product[]>;
  productTypes;
  customerTypes;

  showProductModal = false;

  constructor(private productsService: ProductsService) {
    this.productTypes = env.productTypes;
    this.customerTypes = env.customerTypes;
  }

  ngOnInit() {
    this.fetchProducts();
  }

  fetchProducts() {
    this.products$ = this.productsService.getProducts();
  }
}
