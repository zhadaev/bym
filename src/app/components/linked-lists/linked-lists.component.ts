import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { Product } from '../../shared/product.model';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-linked-lists',
  templateUrl: './linked-lists.component.html',
  styleUrls: ['./linked-lists.component.scss']
})
export class LinkedListsComponent implements OnInit {
  typesForm: FormGroup;
  products: Product[];
  typesSrc: Product[];
  includeSrc = null;
  passSrc;
  includeDest: Product[] = [];
  passDest: Product[] = [];
  result = [];
  @Output() includesChanged = new EventEmitter();

  constructor(
    private productsService: ProductsService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.init();
  }

  init() {
    this.productsService.getProducts()
      .subscribe(
        products => {
          this.products = products;
          this.typesSrc = products;
        }
      );

    this.typesForm = this.fb.group({
      includeSrc: [[]],
      passSrc: [[]],
      includeDest: [[]],
      passDest: [[]]
    });

    this.includeDest = [];
    this.passDest = [];
    this.result = [];
  }

  moveItems(dest, type: string, moveAll = false, revert = false) {
    if (moveAll && !revert) {
      dest = [...dest, ...this.typesSrc];
      this.typesSrc = [];
    } else if (moveAll && revert) {
      this.typesSrc = [...this.typesSrc, ...dest];
      dest = [];
    } else if (!moveAll && revert) {
      const destSelected = this.typesForm.get(type + 'Dest').value;
      dest = dest.filter(p => !destSelected.includes(p.id));
      this.typesSrc = this.typesSrc.length
        ? [...this.typesSrc, ...this.products.filter(product => destSelected.includes(product.id))]
        : this.products.filter(p => destSelected.includes(p.id));
    } else {
      const includeSrcSelected = this.typesForm.get(type + 'Src').value;
      this.typesSrc = this.typesSrc.filter(p => !includeSrcSelected.includes(p.id));
      dest = [...dest, ...this.products.filter(product => includeSrcSelected.includes(product.id))];
    }
    this.typesForm.get(type + 'Src').setValue([]);
    this.typesForm.get(type + 'Dest').setValue([]);
    return dest;
  }

  moveIncludes(all = false, revert = false) {
    this.includeDest = this.moveItems(this.includeDest, 'include', all, revert);
    this.setIncludes();
  }

  movePass(all = false, revert = false) {
    this.passDest = this.moveItems(this.passDest, 'pass', all, revert);
    this.setIncludes();
  }

  setIncludes() {
    let include = [];
    let pass = [];
    if (this.includeDest.length) {
      include = this.includeDest.map(p => {
        return {
          type: 'INCLUDE',
          connectedProduct: p
        };
      });
    }
    if (this.passDest.length) {
      pass = this.passDest.map(p => {
        return {
          type: 'PASS',
          connectedProduct: p
        };
      });
    }
    this.result = [...include, ...pass];
    this.includesChanged.emit(this.result);
  }

  filterOption(filter, name, sku) {
    const _filter = filter.toLowerCase();
    return name.toLowerCase().includes(_filter) || sku.toLowerCase().includes(_filter);
  }
}
