import { Component, OnChanges, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';
import { LinkedListsComponent } from '../linked-lists/linked-lists.component';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit, OnChanges {
  productForm: FormGroup;
  @Input() openModal: boolean;
  @Output() closeModal = new EventEmitter;
  @Output() productCreated = new EventEmitter;
  @ViewChild('linkedLists') linkedLists: LinkedListsComponent;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private productsService: ProductsService,
    private modal: NgxSmartModalService
  ) {}

  ngOnInit() {
    this.productForm = this.fb.group({
      name: ['', Validators.required],
      sku: ['', Validators.required],
      type: ['BASIC_SINGLE', Validators.required],
      customerType: ['ADULT', Validators.required],
      validValue: [{ value: 0, disabled: true }],
      price: [0, Validators.required],
      allowRecharge: [false, Validators.required],
      rechargePrice: [{ value: 0, disabled: true }, Validators.required],
      includes: [[]]
    });

    this.productForm.get('type').valueChanges.subscribe(
      val => {
        val === 'BASIC_SINGLE'
          ? this.productForm.get('validValue').disable()
          : this.productForm.get('validValue').enable();
      }
    );

    this.productForm.get('allowRecharge').valueChanges.subscribe(
      val => {
        if (!val) {
          this.productForm.get('rechargePrice').disable();
          this.productForm.get('rechargePrice').setValue(0);
        } else {
          this.productForm.get('rechargePrice').enable();
        }
      }
    );
  }

  onSubmit() {
    this.productsService.addProduct(this.productForm.getRawValue())
      .subscribe(
        (data) => {
          this.productForm.reset({
            name: '',
            sku: '',
            type: 'BASIC_SINGLE',
            customerType: 'ADULT',
            validValue: 0,
            allowRecharge: false,
            price: 0,
            rechargePrice: 0,
            includes: []
          });
          this.productCreated.emit();
          this.linkedLists.init();
          this.toastr.success('Product was successfully created');
        },
        (err) => {
          this.toastr.error('Error occured. Please, try again');
        }
      );
  }

  onModalClose() {
    this.productForm.reset();
    this.closeModal.emit();
  }

  ngOnChanges() {
    if (this.openModal) {
      this.modal.getModal('createProductModal').open();
    }
  }

  updateIncludes(data) {
    this.productForm.get('includes').setValue(data);
  }

}
