export interface Product {
  id: number;
  sku: string;
  connectedProduct: ConnectedProduct;
}

export interface ConnectedProduct {
  id: number;
  name: string;
  sku: string;
  price: number;
  validValue: number;
  customerType: string;
  type: string;
  includes: any;
  allowRecharge: boolean;
  rechargePrice: number;
}
